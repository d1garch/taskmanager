package ru.pashintsev.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App
{
    public static void main( String[] args ) {
        Project project = new Project();
        Task task = new Task();
        System.out.println("**** WELCOME TO TASK MANAGER ****");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            try {
                switch (reader.readLine()) {
                    case ("project-create"):
                        System.out.println("[PROJECT CREATE]\nENTER NAME:");
                        project.addProject(reader.readLine());
                        System.out.println("[OK]");
                        break;
                    case ("project-remove"):
                        System.out.println("[PROJECT-REMOVE]\nENTER PROJECT NUMBER:");
                        String linePrj = reader.readLine();
                        int indexPrj = Integer.parseInt(linePrj);
                        project.removeProjectById(indexPrj-1);
                        System.out.println("[OK]");
                       break;
                    case ("project-clear") :
                        project.deleteAllProjects();
                        System.out.println("[ALL PROJECTS HAS BEEN DELETED]");
                        break;
                    case ("project-list"):
                        System.out.println("[PROJECT LIST]");
                        if(!project.showAllProjects().isEmpty()) {
                            for (int i = 0; i < project.showAllProjects().size(); i++) {
                                System.out.println((i+1) + ". " + project.showAllProjects().get(i));
                            }
                        }
                        else System.out.println("[PROJECT LIST IS EMPTY]");
                        break;
                    case ("task-create"):
                        System.out.println("[TASK CREATE]\nENTER NAME:");
                        task.addTask(reader.readLine());
                        System.out.println("[OK]");
                        break;
                    case ("task-remove") :
                        System.out.println("[TASK REMOVE]\nENTER TASK NUMBER:");
                        String lineTask = reader.readLine();
                        int indexTask = Integer.parseInt(lineTask);
                        task.removeTaskById(indexTask-1);
                        System.out.println("[OK]");
                        break;
                    case ("task-clear"):
                        task.deleteAllTasks();
                        System.out.println("[ALL TASKS HAS BEEN DELETED]");
                        break;
                    case ("task-list"):
                        System.out.println("[TASK LIST]");
                        if(!task.showAllTasks().isEmpty()) {
                            for (int i = 0; i < task.showAllTasks().size(); i++) {
                                System.out.println((i+1) + ". " + task.showAllTasks().get(i));
                            }
                        }
                        else System.out.println("[TASK LIST IS EMPTY]");
                        break;
                    case ("help"):
                        System.out.println("'project-create' - adds new project to project list\n"+
                                            "'project-list' - shows all projects in project list\n"+
                                            "'project-remove' - removes project from project list by number\n"+
                                            "'project-clear' - clears project list\n"+
                                            "'task-create' - adds new task to task list\n"+
                                            "'task-list' - shows all tasks in task list\n"+
                                            "'task-remove' - removes task from task list by number\n"+
                                            "'task-clear' - clears task list\n"+
                                            "'help' - shows all available commands");
                        break;
                    default:
                        System.out.println("[UNAVAILABLE COMMAND. ENTER 'help' TO SEE COMMAND LIST]");
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
