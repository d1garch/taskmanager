package ru.pashintsev.tm;

import java.util.ArrayList;
import java.util.List;

public class Task {
    private final List<String> tasks;

    public Task () {
        this.tasks = new ArrayList<>();
    }

    public void addTask(String taskName) {
        tasks.add(taskName);
    }

    public void removeTaskByName(String taskName) {
        tasks.remove(taskName);
    }

    public void removeTaskById(int id) {
        tasks.remove(id);
    }

    public List<String> showAllTasks() {
        return tasks;
    }

    public void deleteAllTasks() {
        tasks.clear();
    }
}
