package ru.pashintsev.tm;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private final List<String> projectList;


    public Project() {
        this.projectList = new ArrayList<>();
    }

    public void addProject(String projectName) {
        projectList.add(projectName);
    }

    public void removeProjectByName(String projectName) {
        projectList.remove(projectName);
    }

    public void removeProjectById(int id) {
        projectList.remove(id);
    }

    public List<String> showAllProjects() {
        return projectList;
    }

    public void deleteAllProjects() {
        projectList.clear();
    }




}
